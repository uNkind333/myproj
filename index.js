import $ from 'jquery/dist/jquery.js';

const textPattern = /^[a-zA-Z0-9\s]+/;
const emailPattern = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/i;
const passwordPattern = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
const phonePattern = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
const urlPattern = /^(?:([a-z]+):(?:([a-z]*):)?\/\/)?(?:([^:@]*)(?::([^:@]*))?@)?((?:[a-z0-9_-]+\.)+[a-z]{2,}|localhost|(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])\.){3}(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])))(?::(\d+))?(?:([^:\?\#]+))?(?:\?([^\#]+))?(?:\#([^\s]+))?$/i;

class Validation {
  // check empty string
  isNotEmpty(...strArr) {
    let result;
    strArr.map((item) => {
      item.error.html('');
      if (item.elem.val() === '') {
        const li = $('<li></li>').text('This field is empty');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  // check true length
  isLengthTrue(...strArr) {
    let result;
    strArr.map((item) => {
      if (item.elem.val().length < 5) {
        const li = $('<li></li>').text('This field length less than 5');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  // check is this field valid to text pattern
  validTextPattern(...strArr) {
    let result;
    strArr.map((item) => {
      const value = item.elem.val();
      if (!textPattern.test(value)) {
        const li = $('<li></li>').text('This field is not valid');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  // check is this field valid to email pattern
  validEmailPattern(...strArr) {
    let result;
    strArr.map((item) => {
      const value = item.elem.val();
      if (!emailPattern.test(value)) {
        const li = $('<li></li>').text('This field is not valid');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  // check password
  validPasswordPattern(...strArr) {
    let result;
    const asd = 'asdasd';
    strArr.map((item) => {
      const value = item.elem.val();
      if (!passwordPattern.test(value)) {
        const li = $('<li></li>').text('This password is not valid');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  // check phone
  validPhonePattern(...strArr) {
    let result;
    strArr.map((item) => {
      const value = item.elem.val();
      if (!phonePattern.test(value)) {
        const li = $('<li></li>').text('This phone number is not valid');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  // check url
  validUrlPattern(...strArr) {
    let result;
    strArr.map((item) => {
      const value = item.elem.val();
      if (!urlPattern.test(value)) {
        const li = $('<li></li>').text('This field is not valid');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  // check select
  validSelect(...strArr) {
    let result;
    strArr.every((item) => {
      item.error.html('');
      if (item.elem.val() === '0') {
        const li = $('<li></li>').text('Choose your country');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
  validGender(...strArr) {
    let result;
    strArr.every((item) => {
      item.error.html('');
      if (item.elem.val() === '0') {
        const li = $('<li></li>').text('Choose your gender');
        item.error.append(li);
        result = false;
      } else {
        result = true;
      }
    });
    return result;
  }
}// end of class Validation
const validate = new Validation();
const step = [$('.step1'), $('.step2'), $('.step3')];
let stepCount = 0;
$('.step1').find('.btn-next').on('click', () => {
  const nameObject = {
    elem: $('#text'),
    error: $('#nameError')
  };
  const emailObject = {
    elem: $('#email'),
    error: $('#emailError')
  };
  const passwordObject = {
    elem: $('#password'),
    error: $('#passwordError')
  };

  const step1EmptyValidation = validate.isNotEmpty(nameObject, emailObject, passwordObject);
  const step1LengthValidation = validate.isLengthTrue(nameObject, emailObject);
  const step1PatternTextValidation = validate.validTextPattern(nameObject);
  const step1PatternEmailValidation = validate.validEmailPattern(emailObject);
  const step1PatternPasswordValidation = validate.validPasswordPattern(passwordObject);
  const step1isValid = step1EmptyValidation && step1LengthValidation && step1PatternTextValidation && step1PatternEmailValidation && step1PatternPasswordValidation;
  if (step1isValid) {
    step[stepCount].addClass('hide').removeClass('active');
    stepCount++;
    step[stepCount].addClass('active').removeClass('hide');
  }
});

$('.step2').find('.btn-next').on('click', () => {
  const fullNameObject = {
    elem: $('#fullName'),
    error: $('#fullNameError')
  };
  const phoneObject = {
    elem: $('#phone'),
    error: $('#phoneError')
  };
  const dateObject = {
    elem: $('#date'),
    error: $('#dateError')
  };
  const step2EmptyValidation = validate.isNotEmpty(phoneObject, dateObject, fullNameObject);
  const step2LengthValidation = validate.isLengthTrue(phoneObject, fullNameObject);
  const step2PatternPhoneValidation = validate.validPhonePattern(phoneObject);
  const step2PatternTextValidation = validate.validTextPattern(fullNameObject);
  const step2Valid = step2EmptyValidation && step2LengthValidation && step2PatternPhoneValidation && step2PatternTextValidation;
  if (step2Valid) {
    step[stepCount].addClass('hide').removeClass('active');
    stepCount++;
    step[stepCount].addClass('active');
  }
});

$('.step2').find('.btn-previous').on('click', () => {
  step[stepCount].addClass('hide').removeClass('active');
  stepCount--;
  step[stepCount].addClass('active').removeClass('hide');
});

$('.step3').find('.btn-previous').on('click', () => {
  step[stepCount].addClass('hide').removeClass('active');
  stepCount--;
  step[stepCount].addClass('active').removeClass('hide');
});

$('.step3').find('.btn-next').on('click', () => {
  const selectObj = {
    elem: $('#country'),
    error: $('#countryError')
  };
  const urlObject = {
    elem: $('#url'),
    error: $('#urlError')
  };
  const genderObj = {
    elem: $('#gender'),
    error: $('#genderError')
  };
  const selectValidation = validate.validSelect(selectObj, genderObj);
  const step3EmptyValidation = validate.isNotEmpty(urlObject);
  const urlValidation = validate.validUrlPattern(urlObject);
  const genderValidation = validate.validGender(genderObj);
  if (selectValidation && urlValidation && step3EmptyValidation && genderValidation) {
    $('.btn-next').prop('type', 'submit');
  } else {
    $('.btn-next').prop('type', 'button');
  }
});

document.getElementById('form').addEventListener('submit', (e) => {
  e.preventDefault();
  const obj = {
    login: $('#text').val(),
    email: $('#email').val(),
    password: $('#password').val(),
    fullname: $('#fullName').val(),
    date: $('#date').val(),
    phone: $('#phone').val(),
    country: $('#country :selected').text(),
    gender: $('#gender :selected').text(),
    url: $('#url').val()
  };

  const xhr = new XMLHttpRequest();
  xhr.open('POST', 'http://localhost:3000', false);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  const div = document.createElement('div');
  xhr.onload = function () {
    const obj = JSON.parse(xhr.response);
    const objSite = JSON.parse(obj.site);
    for (const key in objSite) {
      console.log(`${key}: ${objSite[key]}`);
      const div1 = document.createElement('div');
      div1.innerHTML = `${key}: ${objSite[key]}`;
      div.appendChild(div1);
    }
    div.className = 'styled';

    $('.container').hide();
    document.body.appendChild(div);
  };
  console.log(`site=${JSON.stringify(obj)}`);
  xhr.send(`site=${JSON.stringify(obj)}`);
}, false);
